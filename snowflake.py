from turtle import *

reset()
speed(0)
hideturtle()
delay(0)

def side(length=100, level=3):
    if level == 0:
        forward(length)
    else:
        sub_length = length / 3
        sub_level = level - 1

        side(sub_length, sub_level)
        left(60)
        side(sub_length, sub_level)
        right(120)
        side(sub_length, sub_level)
        left(60)
        side(sub_length, sub_level)

def jump(x, y):
    up()
    goto(x, y)
    down()

def flake(l=100, nr_sides=3, level=3):
    angle_turn = 360 / nr_sides

    for _ in range(nr_sides):
        side(l, level)
        right(angle_turn)

clear()
jump(-370, 220)
tracer(0, 0)
flake(750, 3, 7)
update()
